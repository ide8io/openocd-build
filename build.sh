#!/bin/bash -ex

buildver=$(git describe --always --tags --dirty)

OS=${OS:="linux"}

if [ "$OS" == "win" ]; then
  EXE=.exe
  CROSSHOST="--host=i686-w64-mingw32"
  LIBUSB_VERSION=1.0.22

  if [ ! -d "libusb-$LIBUSB_VERSION" ]; then
    mkdir libusb-$LIBUSB_VERSION
    cd libusb-$LIBUSB_VERSION
    curl -L https://github.com/libusb/libusb/releases/download/v$LIBUSB_VERSION/libusb-$LIBUSB_VERSION.7z -o libusb-$LIBUSB_VERSION.7z
    7z x libusb-$LIBUSB_VERSION.7z
    cd -
  fi

  if [ ! -d "pkgconfig" ]; then
    mkdir pkgconfig
  fi

  cat <<EOT > pkgconfig/libusb-1.0.pc
Name: libusb-1.0
Description: C API for USB device access from Linux, Mac OS X, Windows, OpenBSD/NetBSD and Solaris userspace
Version: $LIBUSB_VERSION
Libs: -L$PWD/libusb-$LIBUSB_VERSION/MinGW32/static -lusb-1.0
Libs.private: -ludev  -pthread
Cflags: -I$PWD/libusb-$LIBUSB_VERSION/include/libusb-1.0
EOT

  export PKG_CONFIG_LIBDIR=$PWD/../pkgconfig
fi

PRESERVE_ROOT=--preserve-root
if [ "$(uname)" == "Darwin" ]; then
  PRESERVE_ROOT=
fi

if [ -d "openocd" ]; then
  cd openocd
  git pull
  git reset --hard origin/master
  git clean -f -d
  cd -
else
  git clone git://git.code.sf.net/p/openocd/code openocd
fi

cd openocd
openocdver=$(git describe --always --tags --dirty)+rtt
patch -p1 <../rtt.patch
ARCDIR=openocd-$openocdver
if [ -d "../$ARCDIR" ]; then
  rm -rf $PRESERVE_ROOT ../$ARCDIR/*
fi
./bootstrap
./configure $CROSSHOST \
    --prefix=$PWD/../$ARCDIR \
    --disable-werror
make clean
CFLAGS=-static make
make install
cd -

# Remove any previously build archives
rm $PRESERVE_ROOT openocd-*.tar.bz2 || true

ARCH=-unknown
FTYPE=$(file $ARCDIR/bin/openocd$EXE)
if [[ $FTYPE == *PE32* ]]; then
  ARCH=32
elif [[ $FTYPE == *80386* ]]; then
  ARCH=32
elif [[ $FTYPE == *x86-64* ]]; then
  ARCH=64
elif [[ $FTYPE == *ARM* ]]; then
  AVER=$(readelf -a -W $ARCDIR/bin/openocd | grep Tag_CPU_arch | cut -d " " -f 4)
  ARCH=-arm$AVER
elif [[ $FTYPE == *Mach-O* ]]; then
  OS=macos
  ARCH=
fi

ARCHIVE=$ARCDIR-$OS$ARCH.tar.bz2
cat <<EOT > $ARCDIR/ide8plugin.yaml
name: openocd
exe: bin/openocd$EXE
version: $openocdver
build: $buildver
capabilities:
  rtt:
EOT
tar cjf $ARCHIVE $ARCDIR
rm -rf $PRESERVE_ROOT $ARCDIR

